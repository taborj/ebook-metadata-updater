# eBook Metadata Updater

**metalookup.sh** is a bash shell script that uses the Calibre tools 'fetch-ebook-metadata' and 'ebook-meta' to interactively search for and update eBook metadata.

This was built for my personal collection, so there are some assumptions made (i.e. eBooks have the title in the filename, with spaces replaced by underscores).  If this is different than your situation, feel free to modify the script as you desire.  However, the script will first attempt to use the title and author from existing metadata first.

Upon running, the script will ask for a command to list files.  This command will be passed to the shell, and is designed to be anything you could type at the shell and get a list of files (i.e. 'ls \*.epub' or 'find . -name "\*.mobi"', etc).  

It allows you to skip updating if the returned metadata is incorrect; doing so will log the filename into a file called 'skipped_books.txt' so you can go manually review.

In addition, **ebooksort.sh** is a simple script to move eBook files to folders based on the author tag found in the metadata.