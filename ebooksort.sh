#!/bin/bash

# (C) 2020 Jon Tabor
# taborj@obsolete.site
#
# Move eBook files into folders based on author name

# Ansi color code variables
red="\e[0;91m"
blue="\e[0;94m"
green="\e[0;92m"
white="\e[0;97m"
yellow="\e[0;33m"
reset="\e[0m"

echo -en "${blue}Enter command to list files: ${reset}"
read params
IFS=$'\n'
list=$(eval $params)

for fil in $list;
do
    # Extract the author
    author=$(ebook-meta "$fil" | grep Author | sed 's/.*://'| sed 's/^[ \t]*//')

    # If there is metadata from the file, use it
    if [ -n "$author" ] ;
    then
        # If the folder doesn't exist, create it
        if [ ! -d "$author" ];
        then
           echo -e "${red}Creating folder \"$author\" ${reset}"
           mkdir $author
       fi
       echo -e "${green}Moved file \"$fil\" to folder \"$author\" ${reset}"
       mv "$fil" "$author"/
    fi
done
unset IFS
