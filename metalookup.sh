#!/bin/bash

# (C) 2020 Jon Tabor
# taborj@obsolete.site

# Ansi color code variables
red="\e[0;91m"
blue="\e[0;94m"
green="\e[0;92m"
white="\e[0;97m"
yellow="\e[0;33m"
reset="\e[0m"

echo -en "${red}Enter command to list files: ${reset}"
read params
#IFS=$'\n'; set -f
IFS=$'\n'
list=$(eval $params)

for fil in $list;
do
    # Extract the title from the filename
    book=$(sed 's/_/ /g' <<< $fil)
    book=$(sed 's/.*\///g' <<< $book)
    book=$(sed 's/\..*//g' <<< $book)
    book=$(sed 's/ by.*//g' <<< $book)

    # See if there is existing metadata we can use
    # to get a more accurate match
    title=$(ebook-meta "$fil" | grep Title | sed 's/.*://' | sed 's/^[ \t]*//')
    author=$(ebook-meta "$fil" | grep Author | sed 's/.*://'| sed 's/^[ \t]*//')

    echo -en "${red}Look up metadata for${reset} "$book"${red}?${reset} (Y/N/(T)itle Only/(Q)uit)"
    read input0
    # User wants to do the lookup
    if ([ $input0 == "Y" ] || [ $input0 == "y" ]) || ([ $input0 == "T" ] || [ $input0 == "t" ]);
    then
        echo -e "${green}Looking up metadata for $book ${reset}"
        echo -e "${blue}Filename:${reset} $fil"

        # If there is metadata from the file, use it
        if ([ -n "$title" ] && [ -n "$author" ]) && ([ $input0 != "T" ] && [ $input0 != "t" ]);
        then
            echo "Using title and author from metadata ($title, $author)"
            metadata=$(fetch-ebook-metadata -t \"$title\" -a \"$author\")
            opf=$(fetch-ebook-metadata -o -t \"$title\" -a \"$author\")
        fi
        # If we have an author but no title in the metadata, use the title extracted
        # from the filename
        if [ -z "$title" ] && [ -n "$author" ];
        then
            echo "Using title from filename and author from metadata ($book, $author)"
            metadata=$(fetch-ebook-metadata -t \"$book\" -a \"$author\")
            opf=$(fetch-ebook-metadata -o -t \"$book\" -a \"$author\")
        fi
        # If we have title but no author, or if the user specified to use title
        # only, lookup by just the title from the metadata
        if ([ -n "$title" ] && [ -z "$author" ]) || ([ $input0 == "T" ] || [ $input0 == "t" ]);
        then
            echo "Using title from metadata ($title)"
            metadata=$(fetch-ebook-metadata -t \"$title\")
            opf=$(fetch-ebook-metadata -o -t \"$title\")
        fi
        # If all else fails, look up using the title extracted from the filename
        if [ -z "$title" ] && [ -z "$author" ];
        then
            echo "Using title from filename ($book)"
            metadata=$(fetch-ebook-metadata -t \"$book\")
            opf=$(fetch-ebook-metadata -o -t \"$book\")
        fi
        
        # Isolate the identifier
        echo -en "${blue}Identifier: ${reset}"
        ident=$(echo "$metadata" | grep Identifiers | cut -d ":" -f 2,3 | sed 's/,.*//g')
        echo "$ident"
        # Display the metadata for the user
        echo "$metadata"
        echo -en "${yellow}Update eBook? ${blue}(Y/N/Q)${reset}"
        read input1

        # User says to update the book
        if [ $input1 == "Y" ] || [ $input1 == "y" ];
            then
            #opf=$(fetch-ebook-metadata -o -t "$book" -I $ident)
            echo "$opf" > "$book".opf 
            ebook-meta $fil --from-opf "$book".opf
            rm "$book".opf
        fi

        # User says to skip the book; log the entry
        if [ $input1 == "N" ] || [ $input1 == "n" ];
        then
            echo "$fil" >> skipped_books.txt
        fi
    
        # User says to quit the process
        if [ $input1 == "Q" ] || [ $input1 == "q" ];
        then
            break
        fi
    fi
    # User says to quit the process
    if [ $input0 == "Q" ] || [ $input0 == "q" ];
    then
        break
    fi
done
#unset IFS; set +f
unset IFS
